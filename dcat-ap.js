define({
  "templates": [
    {
      "type": "group",
      "id": "jrc:dcat:OnlyDataset",
      "label": {
        "en": "Dataset",
        "sv": "Dataset"
      },
      "constraints": {
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://www.w3.org/ns/dcat#Dataset"
      },
      "items": [
        {
          "id": "dcat:dcterms:title_da"
        },
        {
          "id": "dcat:dcterms:description_da"
        },
        {
          "id": "dcat:dcterms:language_da"
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "extends": "dcat:theme-isa",
          "choices": [
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/AGRI",
              "label": {
                "en": "Agriculture, fisheries, forestry and food",
                "sv": "Jordbruk, fiske, skogsbruk och livsmedel"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/ECON",
              "label": {
                "en": "Economy and finance",
                "sv": "Ekonomi och finans"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/EDUC",
              "label": {
                "en": "Education, culture and sport",
                "sv": "Utbildning, kultur och sport"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/ENER",
              "label": {
                "en": "Energy",
                "sv": "Energi"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/ENVI",
              "label": {
                "en": "Environment",
                "sv": "Miljö"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/GOVE",
              "label": {
                "en": "Government and public sector",
                "sv": "Regeringen och den offentliga sektorn"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/HEAL",
              "label": {
                "en": "Health",
                "sv": "Hälsa"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/INTR",
              "label": {
                "en": "International issues",
                "sv": "Internationella frågor"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/JUST",
              "label": {
                "en": "Justice, legal system and public safety",
                "sv": "Rättvisa, rättsliga system och allmän säkerhet"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/SOCI",
              "label": {
                "en": "Population and society",
                "sv": "Befolkning och samhälle"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/REGI",
              "label": {
                "en": "Regions and cities",
                "sv": "Regioner och städer"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/TECH",
              "label": {
                "en": "Science and technology",
                "sv": "Vetenskap och teknik"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/data-theme/TRAN",
              "label": {
                "en": "Transport\t",
                "sv": "Transport"
              },
              "description": null
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0
          }
        },
        {
          "id": "dcat:keyword_da"
        },
        {
          "type": "text",
          "nodetype": "DATATYPE_LITERAL",
          "extends": "dcat:dcterms:issued_da",
          "datatype": "http://www.w3.org/2001/XMLSchema#date",
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          },
          "label": {
            "en": "Issue date"
          }
        },
        {
          "id": "dcat:dcterms:modified_da"
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "extends": "dcat:dcterms:accrualPeriodicity_da",
          "choices": [
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/OP_DATPRO",
              "label": {
                "bg": "НЕОКОНЧАТЕЛНИ ДАННИ",
                "cs": "Předběžné údaje",
                "da": "Midlertidige data",
                "de": "Vorläufige Daten",
                "en": "Provisional data",
                "el": "Προσωρινά δεδομένα",
                "es": "Datos provisionales",
                "et": "Esialgsed andmed",
                "fi": "Alustavat tiedot",
                "fr": "Données provisoires",
                "ga": "Sonraí sealadacha",
                "hr": "Privremeni podaci",
                "hu": "Ideiglenes adatok",
                "it": "Dati provvisori",
                "lt": "Laikinieji duomenys",
                "lv": "Provizoriski dati",
                "mt": "Dejta provviżorja",
                "nl": "Voorlopige gegevens",
                "pl": "Dane tymczasowe",
                "pt": "Dados provisórios",
                "ro": "Date provizorii",
                "sk": "Predbežné údaje",
                "sl": "Začasni podatki",
                "sv": "Tillfälliga uppgifter"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/ANNUAL",
              "label": {
                "bg": "годишен",
                "cs": "roční",
                "da": "årligt",
                "de": "jährlich",
                "el": "ετήσιος",
                "en": "annual",
                "et": "aastane",
                "fi": "vuotuinen",
                "fr": "annuel",
                "ga": "bliantúil",
                "hr": "godišnje",
                "hu": "évenkénti",
                "it": "annuale",
                "lv": "reizi gadā",
                "lt": "kasmetinis",
                "mt": "annwali",
                "nl": "jaarlijks",
                "pl": "roczny",
                "pt": "anual",
                "ro": "anual",
                "sk": "ročný",
                "sl": "letni",
                "es": "anual",
                "sv": "årlig"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/BIENNIAL",
              "label": {
                "bg": "двегодишен",
                "cs": "dvouletý",
                "da": "toårigt",
                "de": "zweijährlich",
                "el": "διετής",
                "en": "biennial",
                "et": "iga kahe aasta järel",
                "fi": "kaksivuotinen",
                "fr": "biennal",
                "ga": "dhébhliantúil",
                "hr": "bijenale",
                "hu": "kétévenkénti",
                "it": "biennale",
                "lv": "reizi divos gados",
                "lt": "dvimetis",
                "mt": "biennali",
                "nl": "tweejaarlijks",
                "pl": "dwuletni",
                "pt": "bienal",
                "ro": "bienal",
                "sk": "dvojročný",
                "sl": "bienale",
                "es": "bienal",
                "sv": "vartannat år"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/BIMONTHLY",
              "label": {
                "bg": "двухмесечен",
                "cs": "dvouměsíční",
                "da": "hver anden måned",
                "de": "zweimonatlich",
                "el": "διμηνιαίος",
                "en": "bimonthly",
                "et": "kord kahe kuu jooksul",
                "fi": "kahdesti kuussa",
                "fr": "bimestriel",
                "ga": "gach dhá mhí",
                "hr": "dvomjesečno",
                "hu": "kéthavonkénti",
                "it": "bimestrale",
                "lv": "reizi divos mēnešos",
                "lt": "dviejų mėnesių",
                "mt": "bimensili",
                "nl": "tweemaandelijks",
                "pl": "dwumiesięcznik",
                "pt": "quinzenal",
                "ro": "bilunar",
                "sk": "dvojmesačný",
                "sl": "dvomesečen",
                "es": "bimestral",
                "sv": "varannan månad"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/BIWEEKLY",
              "label": {
                "bg": "на две седмици",
                "cs": "čtrnáctidenní",
                "da": "hver fjortende dag",
                "de": "zweiwöchentlich",
                "el": "δισεβδομαδιαίος",
                "en": "biweekly",
                "et": "kahenädalane",
                "fi": "joka toinen viikko",
                "fr": "tous les quinze jours",
                "ga": "gach coicís",
                "hr": "polumjesečno",
                "hu": "kéthetenkénti",
                "it": "quindicinale",
                "lv": "reizi divās nedēļās",
                "lt": "dvisavaitinis",
                "mt": "ta’ kull ħmistax",
                "nl": "veertiendaags",
                "pl": "dwutygodnik",
                "pt": "quinzenal",
                "ro": "la fiecare două săptămâni",
                "sk": "dvojtýždenný",
                "sl": "vsakih štirinajst dni",
                "es": "quincenal",
                "sv": "var fjortonde dag"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/CONT",
              "label": {
                "bg": "постоянен",
                "cs": "průběžný",
                "da": "kontinuerligt",
                "de": "kontinuierlich",
                "el": "συνεχής",
                "en": "continuous",
                "et": "pidev",
                "fi": "jatkuva",
                "fr": "continuel",
                "ga": "leanúnach",
                "hr": "stalan",
                "hu": "folyamatos",
                "it": "continuo",
                "lv": "pastāvīgs",
                "lt": "nenutrūkstamas",
                "mt": "kontinwu",
                "nl": "voortdurend",
                "pl": "ciągły",
                "pt": "continuo",
                "ro": "continuu",
                "sk": "priebežný",
                "sl": "nenehen",
                "es": "continuo",
                "sv": "kontinuerlig"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/UPDATE_CONT",
              "label": {
                "bg": "постоянно се актуализира",
                "cs": "průběžně aktualizovaný",
                "da": "opdateres løbende",
                "de": "ständige Aktualisierung",
                "el": "ενημερώνεται συνεχώς",
                "en": "continuously updated",
                "et": "ajakohastatakse pidevalt",
                "fi": "päivitetään jatkuvasti",
                "fr": "mise à jour continuelle",
                "ga": "á leasú chun dáta go leanúnach",
                "hr": "stalno ažuriraju",
                "hu": "folyamatosan frissített",
                "it": "in continuo aggiornamento",
                "lv": "pastāvīgi tiek atjaunots",
                "lt": "nuolat atnaujinama",
                "mt": "kontinwament aġġornata",
                "nl": "voortdurend geactualiseerd",
                "pl": "stale aktualizowany",
                "pt": "continuamente atualizado",
                "ro": "actualizare continuă",
                "sk": "priebežne aktualizovaný",
                "sl": "nenehno posodobljen",
                "es": "continuamente actualizado",
                "sv": "uppdateras kontinuerligt"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/DAILY",
              "label": {
                "bg": "ежедневен",
                "cs": "denní",
                "da": "dagligt",
                "de": "täglich",
                "el": "ημερήσιος",
                "en": "daily",
                "et": "päevane",
                "fi": "päivittäin",
                "fr": "quotidien",
                "ga": "laethúil",
                "hr": "dnevni",
                "hu": "naponkénti",
                "it": "quotidiano",
                "lv": "katru dienu",
                "lt": "kasdieninis",
                "mt": "kuljum",
                "nl": "dagelĳks",
                "pl": "dziennik",
                "pt": "diário",
                "ro": "zilnic",
                "sk": "denný",
                "sl": "dnevni",
                "es": "diario",
                "sv": "dagligen"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/IRREG",
              "label": {
                "bg": "неправилен",
                "cs": "nepravidelný",
                "da": "uregelmæssigt",
                "de": "unregelmäßig",
                "el": "μη τακτικός",
                "en": "irregular",
                "et": "ebakorrapärane",
                "fi": "epäsäännöllinen",
                "fr": "irrégulier",
                "ga": "neamhrialta",
                "hr": "neredovit",
                "hu": "rendszertelen",
                "it": "irregolare",
                "lv": "neregulāri",
                "lt": "nereguliarus",
                "mt": "irregolari",
                "nl": "onregelmatig",
                "pl": "nieregularny",
                "pt": "irregular",
                "ro": "neregulat",
                "sk": "nepravidelný",
                "sl": "nepravilen",
                "es": "irregular",
                "sv": "oregelbundet"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/MONTHLY",
              "label": {
                "bg": "месечен",
                "cs": "měsíční",
                "da": "månedligt",
                "de": "monatlich",
                "el": "μηνιαίος",
                "en": "monthly",
                "et": "igakuine",
                "fi": "kuukausittainen",
                "fr": "mensuel",
                "ga": "míosúil",
                "hr": "mjesečno",
                "hu": "havonkénti",
                "it": "mensile",
                "lv": "reizi mēnesī",
                "lt": "kas mėnesį",
                "mt": "mensili",
                "nl": "maandelijks",
                "pl": "miesięcznik",
                "pt": "mensal",
                "ro": "lunar",
                "sk": "mesačný",
                "sl": "mesečen",
                "es": "mensual",
                "sv": "månatligen"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/OTHER",
              "label": {
                "bg": "друг",
                "cs": "ostatní",
                "da": "andet",
                "de": "anderer",
                "el": "άλλο",
                "en": "other",
                "et": "muu",
                "fi": "muut",
                "fr": "autre",
                "ga": "eile",
                "hr": "drugi",
                "hu": "egyéb",
                "it": "altro",
                "lv": "citi",
                "lt": "kitas",
                "mt": "oħra",
                "nl": "overige",
                "pl": "inne",
                "pt": "outro",
                "ro": "altele",
                "sk": "ostatné",
                "sl": "ostali",
                "es": "otro",
                "sv": "andra"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/QUARTERLY",
              "label": {
                "bg": "тримесечен",
                "cs": "čtvrtletní",
                "da": "kvartalsvis",
                "de": "vierteljährlich",
                "el": "τριμηνιαίος",
                "en": "quarterly",
                "et": "kvartaalne",
                "fi": "vuosineljänsittäin",
                "fr": "trimestriel",
                "ga": "ráitheachán",
                "hr": "tromjesečno",
                "hu": "negyedévenkénti",
                "it": "trimestrale",
                "lv": "reizi trīs mēnešos",
                "lt": "kas ketvirtį",
                "mt": "kull tliet xhur",
                "nl": "driemaandelijks",
                "pl": "kwartalnik",
                "pt": "trimestral",
                "ro": "trimestrial",
                "sk": "štvrťročný",
                "sl": "četrtletni",
                "es": "trimestral",
                "sv": "kvartalsvis"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/ANNUAL_2",
              "label": {
                "bg": "два пъти на година",
                "cs": "pololetní",
                "da": "halvårligt",
                "de": "halbjährlich",
                "el": "εξαμηνιαίος",
                "en": "semiannual",
                "et": "pooleaastane",
                "fi": "puolivuotinen",
                "fr": "semestriel",
                "ga": "leathbhliantúil",
                "hr": "polugodišnje",
                "hu": "félévenkénti",
                "it": "semestrale",
                "lv": "reizi pusgadā",
                "lt": "kas pusę metų",
                "mt": "kull sitt xhur",
                "nl": "halfjaarlĳks",
                "pl": "półroczny",
                "pt": "semianual",
                "ro": "semestrial",
                "sk": "polročný",
                "sl": "polletni",
                "es": "semestral",
                "sv": "halvårs"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/MONTHLY_2",
              "label": {
                "bg": "на половин месец",
                "cs": "nědvakrát za měsíc",
                "da": "to gange månedligt",
                "de": "zweimal im Monat",
                "el": "δεκαπενθήμερος",
                "en": "semimonthly",
                "et": "kaks korda kuus",
                "fi": "kaksi kertaa kuukaudessa",
                "fr": "bimensuel",
                "ga": "leath-míosúil",
                "hr": "dvaput mjesečno",
                "hu": "félhavonkénti",
                "it": "bimensile",
                "lv": "divreiz mēnesī",
                "lt": "dukart per mėnesį",
                "mt": "darbtejn fix-xahar",
                "nl": "twee keer per maand",
                "pl": "dwukrotnie w miesiącu",
                "pt": "duas vezes por mês",
                "ro": "se două ori pe lună",
                "sk": "dvakrát mesačne",
                "sl": "dvakrat na mesec",
                "es": "bimensual",
                "sv": "två gånger per månad"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/WEEKLY_2",
              "label": {
                "bg": "дваж на седмица",
                "cs": "dvakrát týdně",
                "da": "to gange ugentligt",
                "de": "zweimal in Woche",
                "el": "διεβδομαδιαίος",
                "en": "semiweekly",
                "et": "kaks korda nädalas",
                "fi": "kaksi kertaa viikossa",
                "fr": "bihebdomadaire",
                "ga": "dhá uair in aghaidh na seachtaine",
                "hr": "dvaput tjedno",
                "hu": "hetente kétszeri",
                "it": "bisettimanale",
                "lv": "divreiz nedēļā",
                "lt": "dukart per savaitę",
                "mt": "darbtejn fil-ġimgħa",
                "nl": "twee keer per week",
                "pl": "dwa razy w tygodniu",
                "pt": "duas vezes por semana",
                "ro": "de două ori pe săptămână",
                "sk": "dvakrát t������������denne",
                "sl": "dvakrat tedensko",
                "es": "bisemanal",
                "sv": "två gånge per vecka"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/MONTHLY_3",
              "label": {
                "bg": "три пъти месечно",
                "cs": "třikrát za měsíc",
                "da": "tre gange månedligt",
                "de": "dreimal im Monat",
                "el": "τρεις φορές τον μήνα",
                "en": "three times a month",
                "et": "kolm korda kuus",
                "fi": "kolme kertaa kuukaudessa",
                "fr": "trois fois par mois",
                "ga": "trí uair sa mhí",
                "hr": "triput mjesečno",
                "hu": "havonta háromszori",
                "it": "tre volte al mese",
                "lv": "trīsreiz mēnesī",
                "lt": "tris kartus per mėnesį",
                "mt": "tliet darbiet fix-xahar",
                "nl": "drie keer per maand",
                "pl": "trzy razy w miesiącu",
                "pt": "três vezes por mês",
                "ro": "de trei ori pe lună",
                "sk": "trikrát mesačne",
                "sl": "trikrat na mesec",
                "es": "tres veces por mes",
                "sv": "tre gånger per månad"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/WEEKLY_3",
              "label": {
                "bg": "три пъти на седмица",
                "cs": "třikrát týdně",
                "da": "tre gange ugentligt",
                "de": "dreimal in Woche",
                "el": "τριεβδομαδιαίος",
                "en": "three times a week",
                "et": "kolm korda nädalas",
                "fi": "kolme kertaa viikossa",
                "fr": "trois fois par semaine",
                "ga": "trí uair in aghaidh na seachtaine",
                "hr": "triput tjedno",
                "hu": "hetente háromszori",
                "it": "tre volte a settimana",
                "lv": "trīsreiz nedēļā",
                "lt": "tris kartus per savaitę",
                "mt": "tliet darbiet fil-ġimgħa",
                "nl": "drie keer per week",
                "pl": "trzy razy w tygodniu",
                "pt": "três vezes por semana",
                "ro": "de trei ori pe săptămână",
                "sk": "trikrát týždenne",
                "sl": "trikrat tedensko",
                "es": "tres veces por semana",
                "sv": "tre gånger per vecka"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/ANNUAL_3",
              "label": {
                "bg": "три пъти на година",
                "cs": "třikrát do roka",
                "da": "tre gange årligt",
                "de": "dreimal im Jahr",
                "el": "τετραμηνιαίος",
                "en": "three times a year",
                "et": "kolm korda aastas",
                "fi": "kolme kertaa vuodessa",
                "fr": "trois fois par an",
                "ga": "trí uair sa bhliain",
                "hr": "triput godišnje",
                "hu": "évente háromszori",
                "it": "tre volte all'anno",
                "lv": "trīsreiz gadā",
                "lt": "tris kartus per metus",
                "mt": "tliet darbiet fis-sena",
                "nl": "drie keer per jaar",
                "pl": "trzy razy w roku",
                "pt": "três vezes por ano",
                "ro": "de trei ori pe an",
                "sk": "trikrát do roka",
                "sl": "trikrat na leto",
                "es": "cuatrimestral",
                "sv": "tre gånger per år"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/TRIENNIAL",
              "label": {
                "bg": "тригодишен",
                "cs": "tříletý",
                "da": "treårligt",
                "de": "dreijährlich",
                "el": "τριετής",
                "en": "triennial",
                "et": "iga kolme aasta järel",
                "fi": "kolmivuotinen",
                "fr": "triennal",
                "ga": "tríbhliantúil",
                "hr": "trijenale",
                "hu": "háromévenkénti",
                "it": "triennale",
                "lv": "reizi trīs gados",
                "lt": "trimetis",
                "mt": "triennali",
                "nl": "driejaarlijks",
                "pl": "trzyletni",
                "pt": "trienal",
                "ro": "trienal",
                "sk": "trojročný",
                "sl": "trienale",
                "es": "trienal",
                "sv": "vart tredje år"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/DAILY_2",
              "label": {
                "bg": "два пъти на ден",
                "cs": "dvakrát denně",
                "da": "to gange om dagen",
                "de": "zweimal täglich",
                "el": "δύο φορές την ημέρα",
                "en": "twice a day",
                "et": "kaks korda päevas",
                "fi": "kahdesti päivässä",
                "fr": "deux fois par jour",
                "ga": "dhá uair sa lá",
                "hr": "dvaput dnevno",
                "hu": "naponta kétszeri",
                "it": "due volte al giorno",
                "lv": "divreiz dienā",
                "lt": "dukart per dieną",
                "mt": "darbtejn kuljum",
                "nl": "tweemaal per dag",
                "pl": "dwa razy dziennie",
                "pt": "duas vezes por dia",
                "ro": "de două ori pe zi",
                "sk": "dvakrát denne",
                "sl": "dvakrat dnevno",
                "es": "dos veces al día",
                "sv": "två gånger per dag"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/UNKNOWN",
              "label": {
                "bg": "непознат",
                "cs": "neznámý",
                "da": "ubekendt",
                "de": "unbekannt",
                "el": "άγνωστος",
                "en": "unknown",
                "et": "tundmatu",
                "fi": "tuntematon",
                "fr": "inconnu",
                "ga": "anaithnid",
                "hr": "nepoznat",
                "hu": "ismeretlen",
                "it": "sconosciuto",
                "lv": "nav zināms",
                "lt": "nežinomas",
                "mt": "mhux magħruf",
                "nl": "onbekend",
                "pl": "nieznany",
                "pt": "desconhecido",
                "ro": "necunoscut",
                "sk": "neznámy",
                "sl": "nepoznan",
                "es": "desconocido",
                "sv": "obekant"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/frequency/WEEKLY",
              "label": {
                "bg": "седмичен",
                "cs": "týdenní",
                "da": "ugentligt",
                "de": "wöchentlich",
                "el": "εβδομαδιαίος",
                "en": "weekly",
                "et": "nädalane",
                "fi": "viikoittainen",
                "fr": "hebdomadaire",
                "ga": "in aghaidh na seachtaine",
                "hr": "tjedni",
                "hu": "hetenkénti",
                "it": "settimanale",
                "lv": "reizi nedēļā",
                "lt": "kas savaitę",
                "mt": "kull ġimgħa",
                "nl": "wekelijks",
                "pl": "tygodnik",
                "pt": "hebdomadário",
                "ro": "săptămânal",
                "sk": "týždenný",
                "sl": "tedensko",
                "es": "semanal",
                "sv": "veckovis"
              }
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          }
        },
        {
          "id": "dcat:landingPage_da"
        },
        {
          "id": "dcat:dcterms:temporal_da"
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "label": {
            "en": "Geographical area"
          },
          "extends": "dcat:dcterms:spatial_da",
          "cardinality": {
            "min": 0,
            "pref": 0
          }
        },
        {
          "type": "group",
          "nodetype": "BLANK",
          "cardinality": {
            "min": 0,
            "pref": 0
          },
          "constraints": {
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://purl.org/dc/terms/Location"
          },
          "items": [
            {
              "type": "text",
              "nodetype": "DATATYPE_LITERAL",
              "cardinality": {
                "min": 0,
                "pref": 1,
                "max": 1
              },
              "datatype": "http://www.opengis.net/ont/geosparql#wktLiteral",
              "property": "http://www.w3.org/ns/locn#geometry",
              "styles": [
                "invisibleGroup"
              ]
            }
          ],
          "property": "http://purl.org/dc/terms/spatial",
          "label": {
            "en": "Bounding box"
          }
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "property": "http://purl.org/dc/terms/publisher",
          "constraints": {
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://xmlns.com/foaf/0.1/Organization"
          },
          "label": {
            "en": "Publisher"
          },
          "choices": [
            {
              "value": "http://publications.europa.eu/resource/authority/corporate-body/JRC",
              "label": {
                "": "European Commission, Joint Research Centre (JRC)"
              },
              "selectable": false
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          },
          "styles": [
            "dropDown"
          ]
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "extends": "dcat:contactPoint-choice_da",
          "cardinality": {
            "min": 1,
            "pref": 0
          }
        },
        {
          "type": "choice",
          "nodetype": "RESOURCE",
          "cardinality": {
            "min": 0,
            "pref": 1
          },
          "constraints": {
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [
              "http://xmlns.com/foaf/0.1/Person",
              "http://xmlns.com/foaf/0.1/Organization"
            ]
          },
          "label": {
            "en": "Contributor"
          },
          "property": "http://purl.org/dc/terms/creator"
        },
        {
          "type": "group",
          "nodetype": "RESOURCE",
          "label": {
            "en": "Publication"
          },
          "property": "http://purl.org/dc/terms/isReferencedBy",
          "items": [
            {
              "type": "text",
              "nodetype": "LANGUAGE_LITERAL",
              "extends": "dcterms:title",
              "cardinality": {
                "min": 1,
                "pref": 0
              }
            },
            {
              "type": "group",
              "nodetype": "RESOURCE",
              "property": "http://purl.org/dc/terms/creator",
              "items": [
                {
                  "type": "text",
                  "nodetype": "ONLY_LITERAL",
                  "extends": "foaf:givenName",
                  "cardinality": {
                    "min": 1,
                    "pref": 0
                  },
                  "label": {
                    "en": "First name"
                  }
                },
                {
                  "type": "text",
                  "nodetype": "ONLY_LITERAL",
                  "extends": "foaf:familyName",
                  "cardinality": {
                    "min": 1,
                    "pref": 0
                  },
                  "label": {
                    "en": "Last name"
                  }
                }
              ],
              "label": {
                "en": "Author"
              },
              "cardinality": {
                "min": 1,
                "pref": 0
              },
              "constraints": {
                "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://xmlns.com/foaf/0.1/Person"
              }
            },
            {
              "type": "text",
              "nodetype": "DATATYPE_LITERAL",
              "extends": "dcterms:issued",
              "datatype": "http://www.w3.org/2001/XMLSchema#gYear",
              "label": {
                "en": "Publication year"
              },
              "cardinality": {
                "min": 0,
                "pref": 1
              }
            },
            {
              "type": "choice",
              "nodetype": "URI",
              "property": "http://purl.org/dc/terms/publisher",
              "constraints": {
                "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://xmlns.com/foaf/0.1/Organization"
              },
              "label": {
                "en": "Publisher"
              },
              "choices": [
                {
                  "value": "http://publications.europa.eu/resource/authority/corporate-body/JRC",
                  "label": {
                    "en": " European Commission, Joint Research Centre (JRC)"
                  },
                  "description": null
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/corporate-body/PUBL",
                  "label": {
                    "en": " Publications Office of the EU (OP)"
                  }
                },
                {
                  "value": "http://aaas.org/",
                  "label": {
                    "en": "American Association for the Advancement of Science (AAAS)"
                  }
                },
                {
                  "value": "http://www.acs.org/",
                  "label": {
                    "en": "American Chemical Society (ACS)"
                  }
                },
                {
                  "value": "https://www.aps.org/",
                  "label": {
                    "en": "American Physical Society (APS)"
                  }
                },
                {
                  "value": "http://www.acm.org/",
                  "label": {
                    "en": "Association for Computing Machinery (ACM)"
                  }
                },
                {
                  "value": "https://www.elsevier.com/",
                  "label": {
                    "en": "Elsevier"
                  }
                },
                {
                  "value": "https://www.ieee.org/",
                  "label": {
                    "en": "Institute of Electrical and Electronics Engineers (IEEE)"
                  }
                },
                {
                  "value": "http://ioppublishing.org/",
                  "label": {
                    "en": "Institute of Physics Publishing"
                  }
                },
                {
                  "value": "http://www.mdpi.com/",
                  "label": {
                    "": "Multidisciplinary Digital Publishing Institute (MDPI)"
                  }
                },
                {
                  "value": "http://www.nature.com/",
                  "label": {
                    "en": "Nature Publishing Group"
                  }
                },
                {
                  "value": "http://www.springer.com/",
                  "label": {
                    "en": "Springer"
                  }
                },
                {
                  "value": "http://taylorandfrancis.com/",
                  "label": {
                    "en": "Taylor & Francis"
                  }
                },
                {
                  "value": "http://www.wiley.com/",
                  "label": {
                    "en": "Wiley & Sons"
                  }
                },
                {
                  "value": "http://arxiv.org/",
                  "label": {
                    "en": "arXiv"
                  }
                },
                {
                  "value": "https://www.plos.org/",
                  "label": {
                    "en": "Public Library of Science (PLOS)"
                  }
                }
              ],
              "cardinality": {
                "min": 0,
                "pref": 1
              },
              "styles": [
                "dropDown"
              ]
            },
            {
              "type": "text",
              "nodetype": "URI",
              "extends": "jrc:dcat:doi",
              "cardinality": {
                "min": 0,
                "pref": 1
              }
            },
            {
              "type": "text",
              "nodetype": "URI",
              "label": {
                "en": "URL"
              },
              "property": "http://www.w3.org/2002/07/owl#sameAs",
              "cardinality": {
                "min": 0,
                "pref": 1
              },
              "pattern": "^(https?|ftp)://[^\\s/$.?#].[^\\s]*$"
            }
          ],
          "constraints": {
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://xmlns.com/foaf/0.1/Document"
          },
          "cardinality": {
            "min": 0,
            "pref": 1
          }
        },
        {
          "type": "group",
          "nodetype": "RESOURCE",
          "label": {
            "en": "Other resource"
          },
          "property": "http://purl.org/dc/terms/relation",
          "items": [
            {
              "type": "text",
              "nodetype": "LANGUAGE_LITERAL",
              "extends": "dcterms:title",
              "cardinality": {
                "min": 1,
                "pref": 0
              }
            },
            {
              "type": "text",
              "nodetype": "LANGUAGE_LITERAL",
              "extends": "dcterms:description",
              "cardinality": {
                "min": 0,
                "pref": 1
              }
            },
            {
              "type": "choice",
              "nodetype": "URI",
              "extends": "jrc:dcterms:format",
              "choices": [
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/AZW",
                  "label": {
                    "en": "Amazon Kindle eBook"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MAP_PRVW",
                  "label": {
                    "en": "ArcGIS Map Preview"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MAP_SRVC",
                  "label": {
                    "en": "ArcGIS Map Service"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ATOM",
                  "label": {
                    "en": "Atom Feed"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/BIN",
                  "label": {
                    "en": "Binary Data"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/BMP",
                  "label": {
                    "en": "Bitmap Image File"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/CSS",
                  "label": {
                    "en": "CSS"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/CSV",
                  "label": {
                    "en": "CSV"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/LAZ",
                  "label": {
                    "en": "Compressed LAS file"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DBF",
                  "label": {
                    "en": "DBF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DCR",
                  "label": {
                    "en": "DCR File"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/E00",
                  "label": {
                    "en": "E00"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ECW",
                  "label": {
                    "en": "ECW"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/EPUB",
                  "label": {
                    "en": "EPUB"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/EPS",
                  "label": {
                    "en": "Encapsulated Postscript"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/GRID_ASCII",
                  "label": {
                    "en": "Esri ASCII grid"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/GDB",
                  "label": {
                    "en": "Esri File Geodatabase"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/REST",
                  "label": {
                    "en": "Esri REST"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SHP",
                  "label": {
                    "en": "Esri Shape"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XLS",
                  "label": {
                    "en": "Excel XLS"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XLSX",
                  "label": {
                    "en": "Excel XLSX"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/FMX2",
                  "label": {
                    "en": "Formex 2"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/FMX3",
                  "label": {
                    "en": "Formex 3"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/FMX4",
                  "label": {
                    "en": "Formex 4"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/GIF",
                  "label": {
                    "en": "GIF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/GML",
                  "label": {
                    "en": "GML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/GZIP",
                  "label": {
                    "en": "GNU zip"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/HDF",
                  "label": {
                    "en": "HDF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/HTML",
                  "label": {
                    "en": "HTML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MSG_HTTP",
                  "label": {
                    "en": "HTTP Message"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/INDD",
                  "label": {
                    "en": "INDD"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/JPEG",
                  "label": {
                    "en": "JPEG"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/JPEG2000",
                  "label": {
                    "en": "JPEG 2000"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/JSON",
                  "label": {
                    "en": "JSON"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/JSON_LD",
                  "label": {
                    "en": "JSON-LD"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/KML",
                  "label": {
                    "en": "KML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/KMZ",
                  "label": {
                    "en": "KMZ"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/LAS",
                  "label": {
                    "en": "LASer file"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MDB",
                  "label": {
                    "en": "MDB"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/METS",
                  "label": {
                    "en": "METS XML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/METS_ZIP",
                  "label": {
                    "en": "METS package"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MOP",
                  "label": {
                    "en": "MOP"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MXD",
                  "label": {
                    "en": "MXD"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TAB",
                  "label": {
                    "en": "MapInfo TAB file"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TAB_RSTR",
                  "label": {
                    "en": "MapInfo TAB raster file"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/MOBI",
                  "label": {
                    "en": "Mobipocket eBook"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/RDF_N_TRIPLES",
                  "label": {
                    "en": "N-Triples"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/NETCDF",
                  "label": {
                    "en": "NetCDF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ODF",
                  "label": {
                    "en": "ODF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ODS",
                  "label": {
                    "en": "ODS"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ODT",
                  "label": {
                    "en": "ODT"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/OVF",
                  "label": {
                    "en": "OVF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/OWL",
                  "label": {
                    "en": "OWL"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/OCTET",
                  "label": {
                    "en": "Octet Stream"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ODC",
                  "label": {
                    "en": "OpenDocument Chart"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ODB",
                  "label": {
                    "en": "OpenDocument Database"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ODG",
                  "label": {
                    "en": "OpenDocument Image"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DMP",
                  "label": {
                    "en": "Oracle Dump"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PDF",
                  "label": {
                    "en": "PDF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PDFA1A",
                  "label": {
                    "en": "PDF/A-1a"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PDFA1B",
                  "label": {
                    "en": "PDF/A-1b"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PDFX",
                  "label": {
                    "en": "PDF/X"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PNG",
                  "label": {
                    "en": "PNG"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PS",
                  "label": {
                    "en": "PS"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PSD",
                  "label": {
                    "en": "PSD"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TXT",
                  "label": {
                    "en": "Plain text"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PPSX",
                  "label": {
                    "en": "PowerPoint PPSX"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PPT",
                  "label": {
                    "en": "PowerPoint PPT"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PPTX",
                  "label": {
                    "en": "PowerPoint PPTX"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/PPS",
                  "label": {
                    "en": "PowerPoint Slide Show"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/OP_DATPRO",
                  "label": {
                    "bg": "НЕОКОНЧАТЕЛНИ ДАННИ",
                    "cs": "Předběžné údaje",
                    "da": "Midlertidige data",
                    "de": "Vorläufige Daten",
                    "en": "Provisional data",
                    "el": "Προσωρινά δεδομένα",
                    "es": "Datos provisionales",
                    "et": "Esialgsed andmed",
                    "fi": "Alustavat tiedot",
                    "fr": "Données provisoires",
                    "ga": "Sonraí sealadacha",
                    "hr": "Privremeni podaci",
                    "hu": "Ideiglenes adatok",
                    "it": "Dati provvisori",
                    "lt": "Laikinieji duomenys",
                    "lv": "Provizoriski dati",
                    "mt": "Dejta provviżorja",
                    "nl": "Voorlopige gegevens",
                    "pl": "Dane tymczasowe",
                    "pt": "Dados provisórios",
                    "ro": "Date provizorii",
                    "sk": "Predbežné údaje",
                    "sl": "Začasni podatki",
                    "sv": "Tillfälliga uppgifter"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/RDF",
                  "label": {
                    "en": "RDF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/RDF_XML",
                  "label": {
                    "en": "RDF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/RSS",
                  "label": {
                    "en": "RSS feed"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/RTF",
                  "label": {
                    "en": "RTF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SDMX",
                  "label": {
                    "en": "SDMX"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SGML",
                  "label": {
                    "en": "SGML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DTD_SGML",
                  "label": {
                    "en": "SGML DTD"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SKOS_XML",
                  "label": {
                    "en": "SKOS"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SPARQLQ",
                  "label": {
                    "en": "SPARQL"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SPARQLQRES",
                  "label": {
                    "en": "SPARQL results"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TAR",
                  "label": {
                    "en": "TAR"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TAR_GZ",
                  "label": {
                    "en": "TAR GZ"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TAR_XZ",
                  "label": {
                    "en": "TAR XZ"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TIFF",
                  "label": {
                    "en": "TIFF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TMX",
                  "label": {
                    "en": "TMX"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/TSV",
                  "label": {
                    "en": "TSV"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/RDF_TURTLE",
                  "label": {
                    "en": "TURTLE"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DOC",
                  "label": {
                    "en": "Word DOC"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DOCX",
                  "label": {
                    "en": "Word DOCX"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/WORLD",
                  "label": {
                    "en": "World file"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XHTML",
                  "label": {
                    "en": "XHTML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XLIFF",
                  "label": {
                    "en": "XLIFF"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XML",
                  "label": {
                    "en": "XML"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/DTD_XML",
                  "label": {
                    "en": "XML DTD"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/SCHEMA_XML",
                  "label": {
                    "en": "XML schema"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XSLFO",
                  "label": {
                    "en": "XSL-FO"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XSLT",
                  "label": {
                    "en": "XSLT"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/XYZ",
                  "label": {
                    "en": "XYZ Chemical File"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/ZIP",
                  "label": {
                    "en": "ZIP"
                  }
                },
                {
                  "value": "http://publications.europa.eu/resource/authority/file-type/GMZ",
                  "label": {
                    "en": "Zipped GML"
                  }
                }
              ]
            },
            {
              "type": "choice",
              "nodetype": "RESOURCE",
              "extends": "jrc:dcterms:accessRights",
              "choices": [
                {
                  "value": "http://data.jrc.ec.europa.eu/accessRights/authorisationRequired",
                  "label": {
                    "en": "authorisation required"
                  }
                },
                {
                  "value": "http://data.jrc.ec.europa.eu/accessRights/noLimitations",
                  "label": {
                    "en": "no limitations"
                  }
                },
                {
                  "value": "http://data.jrc.ec.europa.eu/accessRights/registrationRequired",
                  "label": {
                    "": "registration required"
                  }
                }
              ]
            },
            {
              "type": "choice",
              "nodetype": "URI",
              "extends": "jrc:dcterms:license",
              "choices": [
                {
                  "value": "http://publications.europa.eu/resource/authority/licence/COM_REUSE",
                  "label": {
                    "": " European Commission Reuse and Copyright Notice"
                  }
                },
                {
                  "value": "https://creativecommons.org/licenses/by/4.0/",
                  "label": {
                    "": "Creative Commons Attribution v4"
                  }
                },
                {
                  "value": "https://creativecommons.org/publicdomain/zero/1.0/",
                  "label": {
                    "": "Creative Commons Public Domain Dedication"
                  }
                }
              ]
            },
            {
              "type": "text",
              "nodetype": "LITERAL",
              "extends": "jrc:dcat:doi",
              "cardinality": {
                "min": 0,
                "pref": 1,
                "max": 1
              }
            },
            {
              "type": "text",
              "nodetype": "URI",
              "extends": "jrc:dcat:accessURL",
              "cardinality": {
                "min": 1,
                "pref": 0
              }
            },
            {
              "type": "text",
              "nodetype": "URI",
              "extends": "jrc:dcat:downloadURL",
              "cardinality": {
                "min": 0,
                "pref": 1
              }
            }
          ]
        }
      ]
    },
    {
      "type": "group",
      "id": "jrc:dcat:OnlyDistribution",
      "constraints": {
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://www.w3.org/ns/dcat#Distribution"
      },
      "label": {},
      "items": [
        {
          "type": "text",
          "nodetype": "LANGUAGE_LITERAL",
          "extends": "dcat:dcterms:title_di",
          "cardinality": {
            "min": 1,
            "pref": 0
          }
        },
        {
          "id": "dcat:dcterms:description_di"
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "extends": "jrc:dcterms:format",
          "choices": [
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/AZW",
              "label": {
                "en": "Amazon Kindle eBook"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MAP_PRVW",
              "label": {
                "en": "ArcGIS Map Preview"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MAP_SRVC",
              "label": {
                "en": "ArcGIS Map Service"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ATOM",
              "label": {
                "en": "Atom Feed"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/BIN",
              "label": {
                "en": "Binary Data"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/BMP",
              "label": {
                "en": "Bitmap Image File"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/CSS",
              "label": {
                "en": "CSS"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/CSV",
              "label": {
                "en": "CSV"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/LAZ",
              "label": {
                "en": "Compressed LAS file"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DBF",
              "label": {
                "en": "DBF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DCR",
              "label": {
                "en": "DCR File"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/E00",
              "label": {
                "en": "E00"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ECW",
              "label": {
                "en": "ECW"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/EPUB",
              "label": {
                "en": "EPUB"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/EPS",
              "label": {
                "en": "Encapsulated Postscript"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/GRID_ASCII",
              "label": {
                "en": "Esri ASCII grid"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/GDB",
              "label": {
                "en": "Esri File Geodatabase"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/REST",
              "label": {
                "en": "Esri REST"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SHP",
              "label": {
                "en": "Esri Shape"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XLS",
              "label": {
                "en": "Excel XLS"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XLSX",
              "label": {
                "en": "Excel XLSX"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/FMX2",
              "label": {
                "en": "Formex 2"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/FMX3",
              "label": {
                "en": "Formex 3"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/FMX4",
              "label": {
                "en": "Formex 4"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/GIF",
              "label": {
                "en": "GIF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/GML",
              "label": {
                "en": "GML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/GZIP",
              "label": {
                "en": "GNU zip"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/HDF",
              "label": {
                "en": "HDF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/HTML",
              "label": {
                "en": "HTML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MSG_HTTP",
              "label": {
                "en": "HTTP Message"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/INDD",
              "label": {
                "en": "INDD"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/JPEG",
              "label": {
                "en": "JPEG"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/JPEG2000",
              "label": {
                "en": "JPEG 2000"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/JSON",
              "label": {
                "en": "JSON"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/JSON_LD",
              "label": {
                "en": "JSON-LD"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/KML",
              "label": {
                "en": "KML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/KMZ",
              "label": {
                "en": "KMZ"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/LAS",
              "label": {
                "en": "LASer file"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MDB",
              "label": {
                "en": "MDB"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/METS",
              "label": {
                "en": "METS XML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/METS_ZIP",
              "label": {
                "en": "METS package"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MOP",
              "label": {
                "en": "MOP"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MXD",
              "label": {
                "en": "MXD"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TAB",
              "label": {
                "en": "MapInfo TAB file"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TAB_RSTR",
              "label": {
                "en": "MapInfo TAB raster file"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/MOBI",
              "label": {
                "en": "Mobipocket eBook"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/RDF_N_TRIPLES",
              "label": {
                "en": "N-Triples"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/NETCDF",
              "label": {
                "en": "NetCDF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ODF",
              "label": {
                "en": "ODF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ODS",
              "label": {
                "en": "ODS"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ODT",
              "label": {
                "en": "ODT"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/OVF",
              "label": {
                "en": "OVF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/OWL",
              "label": {
                "en": "OWL"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/OCTET",
              "label": {
                "en": "Octet Stream"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ODC",
              "label": {
                "en": "OpenDocument Chart"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ODB",
              "label": {
                "en": "OpenDocument Database"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ODG",
              "label": {
                "en": "OpenDocument Image"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DMP",
              "label": {
                "en": "Oracle Dump"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PDF",
              "label": {
                "en": "PDF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PDFA1A",
              "label": {
                "en": "PDF/A-1a"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PDFA1B",
              "label": {
                "en": "PDF/A-1b"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PDFX",
              "label": {
                "en": "PDF/X"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PNG",
              "label": {
                "en": "PNG"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PS",
              "label": {
                "en": "PS"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PSD",
              "label": {
                "en": "PSD"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TXT",
              "label": {
                "en": "Plain text"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PPSX",
              "label": {
                "en": "PowerPoint PPSX"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PPT",
              "label": {
                "en": "PowerPoint PPT"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PPTX",
              "label": {
                "en": "PowerPoint PPTX"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/PPS",
              "label": {
                "en": "PowerPoint Slide Show"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/OP_DATPRO",
              "label": {
                "bg": "НЕОКОНЧАТЕЛНИ ДАННИ",
                "cs": "Předběžné údaje",
                "da": "Midlertidige data",
                "de": "Vorläufige Daten",
                "en": "Provisional data",
                "el": "Προσωρινά δεδομένα",
                "es": "Datos provisionales",
                "et": "Esialgsed andmed",
                "fi": "Alustavat tiedot",
                "fr": "Données provisoires",
                "ga": "Sonraí sealadacha",
                "hr": "Privremeni podaci",
                "hu": "Ideiglenes adatok",
                "it": "Dati provvisori",
                "lt": "Laikinieji duomenys",
                "lv": "Provizoriski dati",
                "mt": "Dejta provviżorja",
                "nl": "Voorlopige gegevens",
                "pl": "Dane tymczasowe",
                "pt": "Dados provisórios",
                "ro": "Date provizorii",
                "sk": "Predbežné údaje",
                "sl": "Začasni podatki",
                "sv": "Tillfälliga uppgifter"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/RDF_XML",
              "label": {
                "en": "RDF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/RDF",
              "label": {
                "en": "RDF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/RSS",
              "label": {
                "en": "RSS feed"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/RTF",
              "label": {
                "en": "RTF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SDMX",
              "label": {
                "en": "SDMX"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SGML",
              "label": {
                "en": "SGML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DTD_SGML",
              "label": {
                "en": "SGML DTD"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SKOS_XML",
              "label": {
                "en": "SKOS"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SPARQLQ",
              "label": {
                "en": "SPARQL"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SPARQLQRES",
              "label": {
                "en": "SPARQL results"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TAR",
              "label": {
                "en": "TAR"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TAR_GZ",
              "label": {
                "en": "TAR GZ"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TAR_XZ",
              "label": {
                "en": "TAR XZ"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TIFF",
              "label": {
                "en": "TIFF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TMX",
              "label": {
                "en": "TMX"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/TSV",
              "label": {
                "en": "TSV"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/RDF_TURTLE",
              "label": {
                "en": "TURTLE"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DOC",
              "label": {
                "en": "Word DOC"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DOCX",
              "label": {
                "en": "Word DOCX"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/WORLD",
              "label": {
                "en": "World file"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XHTML",
              "label": {
                "en": "XHTML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XLIFF",
              "label": {
                "en": "XLIFF"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XML",
              "label": {
                "en": "XML"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/DTD_XML",
              "label": {
                "en": "XML DTD"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/SCHEMA_XML",
              "label": {
                "en": "XML schema"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XSLFO",
              "label": {
                "en": "XSL-FO"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XSLT",
              "label": {
                "en": "XSLT"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/XYZ",
              "label": {
                "en": "XYZ Chemical File"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/ZIP",
              "label": {
                "en": "ZIP"
              }
            },
            {
              "value": "http://publications.europa.eu/resource/authority/file-type/GMZ",
              "label": {
                "en": "Zipped GML"
              }
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          }
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "extends": "jrc:dcterms:accessRights",
          "choices": [
            {
              "value": "http://data.jrc.ec.europa.eu/accessRights/authorisationRequired",
              "label": {
                "en": "authorisation required"
              }
            },
            {
              "value": "http://data.jrc.ec.europa.eu/accessRights/noLimitations",
              "label": {
                "en": "no limitations"
              }
            },
            {
              "value": "http://data.jrc.ec.europa.eu/accessRights/registrationRequired",
              "label": {
                "": "registration required"
              }
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          },
          "styles": [
            "dropDown"
          ]
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "extends": "jrc:dcterms:license",
          "choices": [
            {
              "value": "http://publications.europa.eu/resource/authority/licence/COM_REUSE",
              "label": {
                "en": " European Commission Reuse and Copyright Notice"
              }
            },
            {
              "value": "https://creativecommons.org/licenses/by/4.0/",
              "label": {
                "en": "Creative Commons Attribution v4"
              }
            },
            {
              "value": "https://creativecommons.org/publicdomain/zero/1.0/",
              "label": {
                "en": "Creative Commons Public Domain Dedication"
              }
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          },
          "styles": [
            "dropDown"
          ]
        },
        {
          "type": "text",
          "nodetype": "URI",
          "extends": "jrc:dcat:doi",
          "cardinality": {
            "min": 0,
            "pref": 1,
            "max": 1
          }
        },
        {
          "type": "text",
          "nodetype": "URI",
          "extends": "jrc:dcat:accessURL",
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          }
        },
        {
          "type": "text",
          "nodetype": "URI",
          "extends": "jrc:dcat:downloadURL",
          "cardinality": {
            "min": 0,
            "pref": 1,
            "max": 1
          }
        }
      ],
      "cardinality": {
        "min": 0,
        "pref": 0
      },
      "description": {},
      "nodetype": "URI"
    },
    {
      "type": "group",
      "nodetype": "RESOURCE",
      "id": "jrc:dcat:OnlyCatalog",
      "constraints": {
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://www.w3.org/ns/dcat#Catalog"
      },
      "items": [
        {
          "id": "dcat:dcterms:title_ca"
        },
        {
          "type": "text",
          "nodetype": "ONLY_LITERAL",
          "extends": "dcterms:alternative",
          "label": {
            "": "Acronym"
          },
          "cardinality": {
            "min": 1,
            "pref": 0
          }
        },
        {
          "id": "dcat:dcterms:description_ca"
        },
        {
          "type": "choice",
          "nodetype": "URI",
          "property": "http://www.w3.org/ns/dcat#contactPoint",
          "constraints": {
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [
              "http://www.w3.org/2006/vcard/ns#Individual",
              "http://www.w3.org/2006/vcard/ns#Organization"
            ]
          },
          "label": {
            "": "Contact point"
          },
          "cardinality": {
            "min": 1,
            "pref": 0
          }
        },
        {
          "type": "text",
          "nodetype": "URI",
          "extends": "foaf:homepage",
          "cardinality": {
            "min": 0,
            "pref": 1,
            "max": 1
          },
          "pattern": "^(https?|ftp)://[^\\s/$.?#].[^\\s]*$"
        }
      ]
    },
    {
      "type": "group",
      "nodetype": "RESOURCE",
      "id": "jrc:dcat:foaf:Agent",
      "items": [
        {
          "type": "choice",
          "nodetype": "URI",
          "property": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
          "choices": [
            {
              "value": "http://xmlns.com/foaf/0.1/Organization",
              "label": {
                "en": "Organization"
              }
            },
            {
              "value": "http://xmlns.com/foaf/0.1/Person",
              "selectable": false,
              "label": {
                "en": "Person"
              }
            }
          ],
          "cardinality": {
            "min": 1,
            "pref": 0,
            "max": 1
          },
          "label": {
            "en": "Type"
          }
        },
        {
          "type": "text",
          "nodetype": "ONLY_LITERAL",
          "extends": "foaf:name",
          "cardinality": {
            "min": 1,
            "pref": 0
          },
          "deps": [
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://xmlns.com/foaf/0.1/Organization"
          ]
        },
        {
          "type": "text",
          "nodetype": "ONLY_LITERAL",
          "extends": "foaf:givenName",
          "cardinality": {
            "min": 1,
            "pref": 0
          },
          "deps": [
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://xmlns.com/foaf/0.1/Person"
          ],
          "label": {
            "en": "First name"
          }
        },
        {
          "type": "text",
          "nodetype": "ONLY_LITERAL",
          "extends": "foaf:familyName",
          "cardinality": {
            "min": 1,
            "pref": 0
          },
          "deps": [
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://xmlns.com/foaf/0.1/Person"
          ],
          "label": {
            "en": "Last name"
          }
        },
        {
          "type": "text",
          "nodetype": "URI",
          "extends": "foaf:mbox",
          "label": {
            "en": "Email"
          },
          "cardinality": {
            "min": 1,
            "pref": 0
          }
        },
        {
          "type": "text",
          "nodetype": "DATATYPE_LITERAL",
          "extends": "dcterms:identifier",
          "label": {
            "en": "ECAS username"
          },
          "datatype": "http://www.w3.org/2001/XMLSchema#string",
          "cardinality": {
            "min": 0,
            "pref": 1,
            "max": 1
          },
          "deps": [
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://xmlns.com/foaf/0.1/Person"
          ]
        },
        {
          "type": "text",
          "nodetype": "URI",
          "extends": "jrc:dcat:orcid",
          "cardinality": {
            "min": 0,
            "pref": 1,
            "max": 1
          },
          "deps": [
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://xmlns.com/foaf/0.1/Person"
          ]
        }
      ]
    },
    {
      "type": "group",
      "nodetype": "RESOURCE",
      "id": "jrc:dcat:contactPoint",
      "extends": "dcat:contactPoint"
    },
    {
      "type": "text",
      "nodetype": "URI",
      "id": "jrc:dcat:orcid",
      "property": "http://www.w3.org/2002/07/owl#sameAs",
      "pattern": "^\\d{4}-\\d{4}-\\d{4}-\\d{3}[\\dX]$",
      "valueTemplate": "http://orcid.org/",
      "label": {
        "en": "ORCID"
      },
      "cardinality": {
        "min": 0,
        "pref": 0,
        "max": 1
      },
      "description": {
        "en": "Open Researcher and Contributor ID (ORCID) 16-digit number with hyphens, e.g. 0000-0002-1825-0097"
      }
    },
    {
      "type": "text",
      "nodetype": "URI",
      "id": "jrc:dcat:doi",
      "property": "http://www.w3.org/2002/07/owl#sameAs",
      "pattern": "^10[.]\\d{4,}(?:[.]\\d+)*/(?:(?![%\"#? ])\\S)+$",
      "valueTemplate": "http://dx.doi.org/",
      "label": {
        "en": "DOI"
      },
      "cardinality": {
        "min": 0,
        "pref": 0,
        "max": 1
      },
      "description": {
        "en": "Digital Object Identifier (DOI) name, e.g. 10.1029/2012JD017968"
      }
    },
    {
      "type": "choice",
      "nodetype": "URI",
      "id": "jrc:dcterms:format",
      "property": "http://purl.org/dc/terms/format",
      "label": {
        "en": "Format"
      },
      "choices": [
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/AZW",
          "label": {
            "en": "Amazon Kindle eBook"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MAP_PRVW",
          "label": {
            "en": "ArcGIS Map Preview"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MAP_SRVC",
          "label": {
            "en": "ArcGIS Map Service"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ATOM",
          "label": {
            "en": "Atom Feed"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/BIN",
          "label": {
            "en": "Binary Data"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/BMP",
          "label": {
            "en": "Bitmap Image File"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/CSS",
          "label": {
            "en": "CSS"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/CSV",
          "label": {
            "en": "CSV"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/LAZ",
          "label": {
            "en": "Compressed LAS file"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DBF",
          "label": {
            "en": "DBF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DCR",
          "label": {
            "en": "DCR File"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/E00",
          "label": {
            "en": "E00"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ECW",
          "label": {
            "en": "ECW"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/EPUB",
          "label": {
            "en": "EPUB"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/EPS",
          "label": {
            "en": "Encapsulated Postscript"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/GRID_ASCII",
          "label": {
            "en": "Esri ASCII grid"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/GDB",
          "label": {
            "en": "Esri File Geodatabase"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/REST",
          "label": {
            "en": "Esri REST"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SHP",
          "label": {
            "en": "Esri Shape"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XLS",
          "label": {
            "en": "Excel XLS"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XLSX",
          "label": {
            "en": "Excel XLSX"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/FMX2",
          "label": {
            "en": "Formex 2"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/FMX3",
          "label": {
            "en": "Formex 3"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/FMX4",
          "label": {
            "en": "Formex 4"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/GIF",
          "label": {
            "en": "GIF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/GML",
          "label": {
            "en": "GML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/GZIP",
          "label": {
            "en": "GNU zip"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/HDF",
          "label": {
            "en": "HDF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/HTML",
          "label": {
            "en": "HTML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MSG_HTTP",
          "label": {
            "en": "HTTP Message"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/INDD",
          "label": {
            "en": "INDD"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/JPEG",
          "label": {
            "en": "JPEG"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/JPEG2000",
          "label": {
            "en": "JPEG 2000"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/JSON",
          "label": {
            "en": "JSON"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/JSON_LD",
          "label": {
            "en": "JSON-LD"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/KML",
          "label": {
            "en": "KML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/KMZ",
          "label": {
            "en": "KMZ"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/LAS",
          "label": {
            "en": "LASer file"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MDB",
          "label": {
            "en": "MDB"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/METS",
          "label": {
            "en": "METS XML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/METS_ZIP",
          "label": {
            "en": "METS package"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MOP",
          "label": {
            "en": "MOP"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MXD",
          "label": {
            "en": "MXD"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TAB",
          "label": {
            "en": "MapInfo TAB file"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TAB_RSTR",
          "label": {
            "en": "MapInfo TAB raster file"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/MOBI",
          "label": {
            "en": "Mobipocket eBook"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/RDF_N_TRIPLES",
          "label": {
            "en": "N-Triples"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/NETCDF",
          "label": {
            "en": "NetCDF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ODF",
          "label": {
            "en": "ODF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ODS",
          "label": {
            "en": "ODS"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ODT",
          "label": {
            "en": "ODT"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/OVF",
          "label": {
            "en": "OVF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/OWL",
          "label": {
            "en": "OWL"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/OCTET",
          "label": {
            "en": "Octet Stream"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ODC",
          "label": {
            "en": "OpenDocument Chart"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ODB",
          "label": {
            "en": "OpenDocument Database"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ODG",
          "label": {
            "en": "OpenDocument Image"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DMP",
          "label": {
            "en": "Oracle Dump"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PDF",
          "label": {
            "en": "PDF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PDFA1A",
          "label": {
            "en": "PDF/A-1a"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PDFA1B",
          "label": {
            "en": "PDF/A-1b"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PDFX",
          "label": {
            "en": "PDF/X"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PNG",
          "label": {
            "en": "PNG"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PS",
          "label": {
            "en": "PS"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PSD",
          "label": {
            "en": "PSD"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TXT",
          "label": {
            "en": "Plain text"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PPSX",
          "label": {
            "en": "PowerPoint PPSX"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PPT",
          "label": {
            "en": "PowerPoint PPT"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PPTX",
          "label": {
            "en": "PowerPoint PPTX"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/PPS",
          "label": {
            "en": "PowerPoint Slide Show"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/OP_DATPRO",
          "label": {
            "bg": "НЕОКОНЧАТЕЛНИ ДАННИ",
            "cs": "Předběžné údaje",
            "da": "Midlertidige data",
            "de": "Vorläufige Daten",
            "en": "Provisional data",
            "el": "Προσωρινά δεδομένα",
            "es": "Datos provisionales",
            "et": "Esialgsed andmed",
            "fi": "Alustavat tiedot",
            "fr": "Données provisoires",
            "ga": "Sonraí sealadacha",
            "hr": "Privremeni podaci",
            "hu": "Ideiglenes adatok",
            "it": "Dati provvisori",
            "lt": "Laikinieji duomenys",
            "lv": "Provizoriski dati",
            "mt": "Dejta provviżorja",
            "nl": "Voorlopige gegevens",
            "pl": "Dane tymczasowe",
            "pt": "Dados provisórios",
            "ro": "Date provizorii",
            "sk": "Predbežné údaje",
            "sl": "Začasni podatki",
            "sv": "Tillfälliga uppgifter"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/RDF",
          "label": {
            "en": "RDF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/RDF_XML",
          "label": {
            "en": "RDF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/RSS",
          "label": {
            "en": "RSS feed"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/RTF",
          "label": {
            "en": "RTF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SDMX",
          "label": {
            "en": "SDMX"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SGML",
          "label": {
            "en": "SGML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DTD_SGML",
          "label": {
            "en": "SGML DTD"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SKOS_XML",
          "label": {
            "en": "SKOS"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SPARQLQ",
          "label": {
            "en": "SPARQL"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SPARQLQRES",
          "label": {
            "en": "SPARQL results"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TAR",
          "label": {
            "en": "TAR"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TAR_GZ",
          "label": {
            "en": "TAR GZ"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TAR_XZ",
          "label": {
            "en": "TAR XZ"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TIFF",
          "label": {
            "en": "TIFF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TMX",
          "label": {
            "en": "TMX"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/TSV",
          "label": {
            "en": "TSV"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/RDF_TURTLE",
          "label": {
            "en": "TURTLE"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DOC",
          "label": {
            "en": "Word DOC"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DOCX",
          "label": {
            "en": "Word DOCX"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/WORLD",
          "label": {
            "en": "World file"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XHTML",
          "label": {
            "en": "XHTML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XLIFF",
          "label": {
            "en": "XLIFF"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XML",
          "label": {
            "en": "XML"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/DTD_XML",
          "label": {
            "en": "XML DTD"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/SCHEMA_XML",
          "label": {
            "en": "XML schema"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XSLFO",
          "label": {
            "en": "XSL-FO"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XSLT",
          "label": {
            "en": "XSLT"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/XYZ",
          "label": {
            "en": "XYZ Chemical File"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/ZIP",
          "label": {
            "en": "ZIP"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/file-type/GMZ",
          "label": {
            "en": "Zipped GML"
          }
        }
      ]
    },
    {
      "type": "choice",
      "nodetype": "URI",
      "id": "jrc:dcterms:license",
      "property": "http://purl.org/dc/terms/license",
      "label": {
        "en": "Licence"
      },
      "choices": [
        {
          "value": "https://creativecommons.org/licenses/by/4.0/",
          "label": {
            "en": "Creative Commons Attribution v4"
          }
        },
        {
          "value": "https://creativecommons.org/publicdomain/zero/1.0/",
          "label": {
            "en": "Creative Commons Public Domain Dedication"
          }
        },
        {
          "value": "http://publications.europa.eu/resource/authority/licence/COM_REUSE",
          "label": {
            "en": " European Commission Reuse and Copyright Notice"
          }
        }
      ]
    },
    {
      "type": "text",
      "nodetype": "URI",
      "id": "jrc:dcat:accessURL",
      "pattern": "^(https?|ftp)://[^\\s/$.?#].[^\\s]*$",
      "extends": "dcat:accessURL_di",
      "cardinality": {
        "min": 0,
        "pref": 1
      }
    },
    {
      "type": "text",
      "nodetype": "URI",
      "id": "jrc:dcat:downloadURL",
      "extends": "dcat:downloadURL_di",
      "pattern": "^(https?|ftp)://[^\\s/$.?#].[^\\s]*$"
    },
    {
      "type": "choice",
      "nodetype": "URI",
      "id": "jrc:dcterms:accessRights",
      "extends": "dcterms:accessRights",
      "label": {
        "en": "Access restrictions"
      },
      "choices": [
        {
          "value": "http://data.jrc.ec.europa.eu/accessRights/authorisationRequired",
          "label": {
            "en": "authorisation required"
          }
        },
        {
          "value": "http://data.jrc.ec.europa.eu/accessRights/noLimitations",
          "label": {
            "en": "no limitations"
          }
        },
        {
          "value": "http://data.jrc.ec.europa.eu/accessRights/registrationRequired",
          "label": {
            "": "registration required"
          }
        }
      ],
      "cardinality": {
        "min": 0,
        "pref": 0
      }
    }
  ]
});